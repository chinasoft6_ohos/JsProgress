import Animator from "@ohos.animator";

export default {
    props: [
        'progress',
        'backgroundColor',
        'progressColor'
    ],
    data: {
        display: false,
        animDuration: 800,
        backgroundColor: "#c21135",
        progressColor: "#6bac33",
        circleWidth: 20,
        angle: 0,
        progressText: 0,
        ctx: null,
        centerX: 0,
        centerY: 0,
        width: 0,
        height: 0,
        animator: null,
        auto: true
    },
    onInit() {
        var options = {
            duration: this.animDuration,
            direction: 'alternate-reverse',
            easing: 'linear',
            fill: 'forwards',
            iterations: 1,
            begin: 0,
            end: 360.0
        };
        var _this = this
        this.animator = Animator.createAnimator(options)
        this.animator.onframe = function (value) {
            _this.angle = value
            _this.draw(_this.ctx)
        }
        this.$watch('progress', 'onProgressChanged')
        this.$watch('backgroundColor', 'onBackgroundChanged')
        this.$watch('progressColor', 'onForegroundChanged')
        requestAnimationFrame(function () {
            _this.initWidget()
            _this.draw(_this.ctx)
        })
    },
    onBackgroundChanged(oldV, newV) {
        this.backgroundColor = newV
    },
    onForegroundChanged(oldV, newV) {
        this.progressColor = newV
    },
    onProgressChanged(oldV, newV) {
        console.log("onProgressChanged from:" + oldV + " to: " + newV)
        this.initWidget()
        if (oldV >= 1) {
            oldV = 1
        }
        if (newV >= 1) {
            newV = 1
        }
        var options = {
            duration: this.animDuration,
            direction: 'alternate-reverse',
            easing: 'linear',
            fill: 'forwards',
            iterations: 1,
            begin: oldV * 360,
            end: newV * 360
        };
        this.animator.update(options)
        this.animator.play()
    },
    initWidget() {
        console.log("init widget")
        if (this.ctx === null) {
            let widget = this.$element('progress-bar');
            this.ctx = widget.getContext('2d', {
                antialias: true
            })
            this.width = widget.getBoundingClientRect().width
            this.height = widget.getBoundingClientRect().height
            this.centerX = widget.getBoundingClientRect().left + this.width / 2
            this.centerY = widget.getBoundingClientRect().top + this.height / 2
            console.log("canvas size = " + this.width + ", " + this.height)
            console.log("canvas center = " + this.centerX + ", " + this.centerY)
        }
    },
    onTouchEvent(ev) {
        this.initWidget()
        let x = ev.touches[0].globalX - this.centerX
        let y = ev.touches[0].globalY - this.centerY
        let tan = x / y
        if (x > 0 && y < 0) {
            this.angle = -Math.atan(tan) * 180 / Math.PI
        } else if (x > 0 && y > 0) {
            this.angle = 180 - Math.atan(tan) * 180 / Math.PI
        } else if (x < 0 && y > 0) {
            this.angle = 180 - Math.atan(tan) * 180 / Math.PI
        } else {
            this.angle = 360 - Math.atan(tan) * 180 / Math.PI
        }
        console.log("angle = " + this.angle)
        if (this.auto) {
            this.animator.play()
        } else {
            requestAnimationFrame(this.draw(this.ctx))
        }
    },
    draw(ctx) {
        this.display = true
        ctx.lineWidth = this.circleWidth
        ctx.lineCap = 'round'
        // ctx可以理解为canvas + paint
        ctx.clearRect(0, 0, this.width, this.height) // 会闪屏，渲染问题
        ctx.save() // save1
        ctx.translate(this.width / 2, this.height / 2)
        // draw background
        ctx.beginPath()
        ctx.strokeStyle = this.backgroundColor
        ctx.arc(0, 0, 100, 0, 2 * Math.PI) // r = 100, 参数是弧度，角度 = 弧度 / PI * 180
        ctx.stroke() // 绘制
        ctx.closePath()
        // draw progress
        ctx.save()
        ctx.rotate(-90 / 180 * Math.PI)
        ctx.beginPath()
        ctx.strokeStyle = this.progressColor
        ctx.arc(0, 0, 100, 0, this.angle / 180 * Math.PI) // r = 100, 参数是弧度，角度 = 弧度 / PI * 180
        ctx.stroke() // 绘制
        ctx.closePath()
        ctx.restore()
        ctx.restore() // save1
        this.notifyChanged()
    },
    notifyChanged() {
        this.$emit("currentAngle", this.angle)
        this.$emit("currentProgress", Math.ceil(this.angle / 3.6) / 100)
        this.progressText = Math.ceil(this.angle / 3.6) + "%"
    },
}
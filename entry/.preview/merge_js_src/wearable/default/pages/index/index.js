export default {
    data: {
        number: 0,
        progress: 0
    },
    onInit() {
    },
    onClick() {
        let p = this.number / 100
        if (p > 1) {
            p = 1
        }
        this.progress = p
    },
    numberChanged(e) {
        this.number = e.value
    },
    onAngleChanged(angle) {
        console.log("onAngleChanged: " + angle.detail)
    },
    onProgressChanged(progress) {
        console.log("onProgressChanged: " + progress.detail)
    }
}
